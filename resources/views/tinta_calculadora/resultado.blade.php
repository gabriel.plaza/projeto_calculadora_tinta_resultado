<!DOCTYPE html>
<html>

<head>
    <title>Resultado</title>
    <style>
        /* Estilo básico para a exibição do resultado */
        body {
            font-family: Arial, sans-serif;
            background-color: #f9f9f9;
        }

        .container {
            width: 80%;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            margin-top: 0;
            text-align: center;
            color: #333;
        }

        .tabela {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
        }

        .tabela th,
        .tabela td {
            padding: 12px;
            text-align: left;
            border-bottom: 1px solid #ddd;
            color: #555;
        }

        .tabela th {
            background-color: #f2f2f2;
        }

        .total-litros,
        .total-area {
            margin-top: 20px;
            font-weight: bold;
            color: #333;
        }

        .button {
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            background-color: #008CBA;
        }

      

        /* Blue */
    </style>
</head>

<body>
    <div class="container">
        <h1>Resultados</h1>
        <table class="tabela">
            <thead>
                <tr>
                    <th>Tipo de Tinta</th>
                    <th>Quantidade de Latas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($latasComprar as $tipo => $quantidade)
                    @if ($quantidade > 0)
                        <tr>
                            <td>{{ $tipo }}L</td>
                            <td>{{ $quantidade }}</td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="total-litros">
            <p>Total de litros: {{ $totalLitrosTotal }}L</p>
        </div>
        <div class="total-area">
            <p>Total da área: {{ $totalArea }}m²</p>
        </div>
    <div>
    </div>
    <a class="button" href="/">Voltar</a>
    </div>
</body>

</html>
