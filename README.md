Descrição do Projeto ⭐

Uma aplicação web que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.

Tela Inicial para Calculo de Tinta

![Terminal](https://cdn.discordapp.com/attachments/682384077072105487/1094114744010551477/image.png)
![Terminal](https://cdn.discordapp.com/attachments/682384077072105487/1094114795118141460/image.png)



Tela Resultado Calculo de Tinta

![Terminal](https://cdn.discordapp.com/attachments/682384077072105487/1094114851720273960/image.png)




Requisitos
PHP 7.2 ou superior
Composer version 2.0.9 ou superior


Como rodar a aplicação ▶️

No terminal, clone o projeto:

git clone https://gitlab.com/gabriel.plaza/projeto_calculadora_tinta_resultado
Entre na pasta do projeto com a IDE:

Instale as dependências:

composer install


Execute a aplicação e abra a porta que aparecer na web:

 php artisan serve
 
http://127.0.0.1:8000/


Tecnologias 💻
